# be flat tiger

## Songbook herunterladen

* [Einspaltiges Layout](https://gitlab.com/ukulele_songbook/ukulele-songbook/-/raw/master/setlists/songbook/songbook.pdf?inline=false)
* [Zweispaltiges Layout](https://gitlab.com/ukulele_songbook/ukulele-songbook/-/raw/master/setlists/songbook/songbook_2.pdf?inline=false)

## Entwicklung

### Struktur

* Alle Liedtexte im Unterorder `songs`
* Setlists (Liedauswahl) und Songbook (alle Lieder) greifen auf jeweilige Liedtexte im Ordner `songs` zu
* Songbook im Unterordner `setlists/songbook` als einspaltige und zweispaltige PDF
* Setlists von Auftritten im Unterordner `setlists/Datum Ort`
* Persönliche Setlist (wird nicht hochgeladen) im Unterordner `setlists/songbook_privat`

### Dokument erstellen

* Ggf. Spaltenanzahl in `songbook.tex` anpassen
* `pdflatex songbook.tex` ausführen

### Git Bedienung

* Befehl um das Repository zu klonen: `git clone git@gitlab.com:ukulele_songbook/ukulele-songbook.git`
* Hinzufügen einer neuen Datei: `git add Datei`
* Änderungen zusammenfassen/abschließen: `git commit`
* Änderungen hierhin hochladen: `git push`